"use strict";

/**
 * @constructor
 */
function SqlModelRelationMapping() {
  /**
   * The `SqlModel` subclass of the related model or a path to file that exports one.
   *
   * @type {Function|String}
   */
  this.modelClass = null;

  /**
   * The relation class.
   *
   * Must be a constructor of a subclass of `SqlModelRelation`. You can use the
   * shortcuts in `SqlModel` for simple relations:
   *
   * ```
   * SqlModel.HasOneRelation
   * SqlModel.HasManyRelation
   * SqlModel.ManyToManyRelation
   * ```
   *
   * @type {SqlModelRelation}
   */
  this.relation = null;

  /**
   * Custom join information.
   *
   * table: The join table. Only needed for many-to-many relations.
   * ownerIdColumn: The name of the column that contains the identifier of the model class that owns the relation.
   * relatedIdColumn The name of the column that contains the identifier of the related model class.
   *
   * This is optional. See `SqlModelRelation.joinColumnName` and `SqlModelRelation.joinTableName` for
   * default values.
   *
   * @type {{table:String, ownerIdColumn:String, relatedIdColumn: String}}
   */
  this.join = null;

  /**
   * Overrides this.join.ownerIdColumn and this.join.relatedIdColumn.
   *
   * Makes it cleaner to create HasOne and HasMany relations.
   *
   * @type {String}
   */
  this.joinColumn = null;

  /**
   * Optional object or function that modifies the relation fetch query.
   *
   * Can be either a hash of key value pairs or a function that takes an `SqlModelQueryBuilder` as parameter.
   *
   * Examples:
   *
   * ```js
   * {
   *   someProperty: 10,
   *   someOtherProperty: 'foo'
   * }
   *
   * function(queryBuilder) {
   *   queryBuilder.where('someProperty', 10).andWhere('someOtherProperty', 'foo');
   * }
   * ```
   *
   * @type {object|function(SqlModelQueryBuilder)}
   */
  this.query = null;
};

module.exports = SqlModelRelationMapping;
