"use strict";

var _ = require('lodash')
  , classUtils = require('../../../class-utils')
  , SqlModelQueryBuilder = require('../SqlModelQueryBuilder')
  , SqlModelRelation = require('./SqlModelRelation');

/**
 * @constructor
 * @extends SqlModelRelation
 */
function HasOneSqlModelRelation() {
  SqlModelRelation.apply(this, arguments);
}

classUtils.inherits(HasOneSqlModelRelation, SqlModelRelation);

/**
 * @override
 */
HasOneSqlModelRelation.prototype.find = function (model) {
  var self = this
    , joinId = model[this.relatedJoinColumn]
    , builder = null;

  if (joinId === null || joinId === void 0) {
    builder = SqlModelQueryBuilder.returning(null);
  } else {
    builder = this.relatedModelClass
      .findOne()
      .where('id', joinId)
      .runFunction(this.additionalQuery);
  }

  return builder.runAfterExecution(function (relationModel) {
    model[self.name] = relationModel || null;
    return relationModel;
  });
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.findForMany = function (models) {
  var self = this
    , builder = null
    , modelsByJoinId = {}
    , joinIds;

  if (_.isEmpty(models)) {
    return SqlModelQueryBuilder.returning([]);
  }

  _.each(models, function (model) {
    model[self.name] = null;
    var joinId = model[self.relatedJoinColumn];
    if (joinId) {
      if (!modelsByJoinId[joinId]) { modelsByJoinId[joinId] = []; }
      modelsByJoinId[joinId].push(model);
    }
  });

  joinIds = _.keys(modelsByJoinId);
  if (_.isEmpty(joinIds)) {
    builder = SqlModelQueryBuilder.returning([]);
  } else {
    builder = this.relatedModelClass
      .findWhereIn('id', joinIds)
      .runFunction(this.additionalQuery);
  }

  return builder.runAfterExecution(function (relationModels) {
    return _.map(relationModels, function (relationModel) {
      var joinId = relationModel.id;
      var models = modelsByJoinId[joinId];
      for (var i = 0, l = models.length; i < l; ++i) {
        models[i][self.name] = relationModel;
      }
      return relationModel;
    });
  });
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.findOne = function (model) {
  return this.find(model);
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.insert = function (model, modelToInsert) {
  var self = this;
  return this.relatedModelClass
    .insert(modelToInsert)
    .enqueueBuilder(function (insertedModel) {
      model[self.relatedJoinColumn] = insertedModel.id;
      model[self.name] = insertedModel;
      return model.$update().runAfterExecution(function () {
        return insertedModel;
      });
    });
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.del = function (model) {
  var self = this;
  var joinId = model[self.relatedJoinColumn];
  model[self.relatedJoinColumn] = null;
  model[self.name] = null;
  return model.$update().enqueueBuilder(function () {
    return self.relatedModelClass.deleteById(joinId);
  });
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.bind = function (model, idOfModelToBind) {
  var self = this;
  return this.relatedModelClass
    .findById(idOfModelToBind)
    .enqueueBuilder(function (modelToBind) {
      model[self.relatedJoinColumn] = modelToBind ? modelToBind.id : null;
      model[self.name] = modelToBind || null;
      return model.$update().runAfterExecution(function () {
        return modelToBind;
      });
    });
};

/**
 * @override
 */
HasOneSqlModelRelation.prototype.unbind = function (model) {
  var self = this;
  return this.relatedModelClass
    .findById(model[self.relatedJoinColumn])
    .enqueueBuilder(function (modelToUnBind) {
      model[self.relatedJoinColumn] = null;
      model[self.name] = null;
      return model.$update().runAfterExecution(function () {
        return modelToUnBind;
      });
    });
};

module.exports = HasOneSqlModelRelation;
