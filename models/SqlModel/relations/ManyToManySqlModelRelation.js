"use strict";

var _ = require('lodash')
  , classUtils = require('../../../class-utils')
  , arrayUtils = require('../../../utils/array-utils')
  , SqlModelQueryBuilder = require('../SqlModelQueryBuilder')
  , SqlModelRelation = require('./SqlModelRelation');

/**
 * @constructor
 * @extends SqlModelRelation
 */
function ManyToManySqlModelRelation() {
  SqlModelRelation.apply(this, arguments);
}

classUtils.inherits(ManyToManySqlModelRelation, SqlModelRelation);

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.find = function (model) {
  var self = this;
  return this.relatedModelClass
    .find()
    .whereIn('id', function () {
      this.select(self.relatedJoinColumn).from(self.joinTable).where(self.ownerJoinColumn, model.id);
    })
    .runFunction(this.additionalQuery)
    .runAfterExecution(function (relationModels) {
      model[self.name] = relationModels;
      return relationModels;
    });
};

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.findForMany = function (models) {
  var self = this
    , modelsById = {}
    , table = self.relatedModelClass.tableName
    , joinTable = self.joinTable
    , joinIds
    , chunkedJoinIds;

  if (_.isEmpty(models)) {
    return SqlModelQueryBuilder.returning([]);
  }

  _.each(models, function (model) {
    model[self.name] = [];
    var id = model.id;
    if (!modelsById[id]) { modelsById[id] = []; }
    modelsById[id].push(model);
  });

  joinIds = _.keys(modelsById);
  chunkedJoinIds = arrayUtils.chunked(joinIds, 500);

  return SqlModelQueryBuilder
    .all(_.map(chunkedJoinIds, function (joinIdChunk) {
      return SqlModelQueryBuilder
        .forClass(self.relatedModelClass)
        .select(as(dot(joinTable, self.ownerJoinColumn), '$tempId'), dot(table, '*'))
        .from(joinTable)
        .join(table, dot(table, 'id'), '=', dot(joinTable, self.relatedJoinColumn))
        .whereIn(dot(joinTable, self.ownerJoinColumn), joinIdChunk)
        .runFunction(self.additionalQuery);
    }))
    .runAfterExecution(function (relationModels) {
      return _.map(relationModels, function (relationModel) {
        var joinId = relationModel.$tempId
          , models = modelsById[joinId];
        for (var i = 0, l = models.length; i < l; ++i) {
          models[i][self.name].push(relationModel);
        }
        return relationModel;
      });
    });
};

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.insert = function (model, modelToInsert) {
  var self = this;
  return this.relatedModelClass
    .insert(modelToInsert)
    .enqueueBuilder(function (insertedModel) {
      var joinRow = self.joinRow(model.id, insertedModel.id);
      return SqlModelQueryBuilder
        .forClass(self.relatedModelClass)
        .table(self.joinTable)
        .insert(joinRow)
        .runAfterExecution(function () {
          return insertedModel;
        });
    })
    .runAfterExecution(function (insertedModel) {
      model[self.name] = model[self.name] || [];
      model[self.name].push(insertedModel);
      return insertedModel;
    });
};

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.del = function (model, idOfModelToDelete) {
  var self = this;
  var joinRow = this.joinRow(model.id, idOfModelToDelete);
  return SqlModelQueryBuilder
    .forClass(self.relatedModelClass)
    .table(self.joinTable)
    .delete()
    .where(joinRow)
    .enqueueBuilder(function () {
      return self.relatedModelClass.deleteById(idOfModelToDelete);
    })
    .runAfterExecution(function (input) {
      _.remove(model[self.name], {id: idOfModelToDelete});
      return input;
    });
};

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.bind = function (model, idOfModelToBind) {
  var self = this;
  return this.relatedModelClass
    .findById(idOfModelToBind)
    .enqueueBuilder(function (modelToBind) {
      var joinRow = self.joinRow(model.id, idOfModelToBind);
      return SqlModelQueryBuilder
        .forClass(self.relatedModelClass)
        .table(self.joinTable)
        .insert(joinRow)
        .runAfterExecution(function () {
          return modelToBind;
        });
    })
    .runAfterExecution(function (modelToBind) {
      model[self.name] = model[self.name] || [];
      model[self.name].push(modelToBind);
      return modelToBind;
    });
};

/**
 * @override
 */
ManyToManySqlModelRelation.prototype.unbind = function (model, idOfModelToUnBind) {
  var self = this;
  return this.relatedModelClass
    .findById(idOfModelToUnBind)
    .enqueueBuilder(function (modelToUnbind) {
      var joinRow = self.joinRow(model.id, idOfModelToUnBind);
      return SqlModelQueryBuilder
        .forClass(self.relatedModelClass)
        .table(self.joinTable)
        .delete()
        .where(joinRow)
        .runAfterExecution(function () {
          return modelToUnbind;
        });
    })
    .runAfterExecution(function (modelToUnbind) {
      _.remove(model[self.name], {id: idOfModelToUnBind});
      return modelToUnbind;
    });
};

function dot(left, right) {
  return left + '.' + right;
}

function as(left, right) {
  return left + ' as ' + right;
}

module.exports = ManyToManySqlModelRelation;
