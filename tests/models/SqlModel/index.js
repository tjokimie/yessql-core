describe('SqlModel', function() {

  var shared = {
    db: require('knex')({
      debug: false,
      client: 'sqlite3',
      connection: {
        filename: ':memory:'
      }
    })
  };

  require('./basic')(shared);
  require('./create-models')(shared);
  require('./create-tables')(shared);
  require('./create')(shared);
  require('./read')(shared);
  require('./update')(shared);
  require('./delete')(shared);
  require('./read-performance')(shared);

});
